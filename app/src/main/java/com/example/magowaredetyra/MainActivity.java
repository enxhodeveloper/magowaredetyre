package com.example.magowaredetyra;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private List<Program> channel1Programmes;
    private List<Program> channel2Programmes;
    private Program publicitet;

    private TextView channel1;
    private TextView channel2;

    private Handler channel1Handler;
    private Handler channel2Handler;

    private int channel1Iterator;
    private int channel2Iterator;


    //In order to have real life timing this number must be set to 60000 (1min == 60000millis)
    //for testing purposes can be set much lower like 100 or 10
    private static final int COIF = 100;
    private static final String TAG = "MAIN_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getViews();
        fillProgramLists();
        startTransmitting();
    }

    private void fillProgramLists(){
        channel1Programmes = new ArrayList<>();
        channel2Programmes = new ArrayList<>();

        publicitet = new Program("PUB", 2);

        channel1Programmes.add(new Program("A", 90));
        channel1Programmes.add(new Program("C", 100));
        channel1Programmes.add(new Program("D", 10));
        channel1Programmes.add(new Program("E", 85));
        channel1Programmes.add(new Program("F", 120));
        channel1Programmes.add(new Program("GJ", 58));
        channel1Programmes.add(new Program("I", 79));
        channel1Programmes.add(new Program("K", 150));
        channel1Programmes.add(new Program("LL", 91));

        channel2Programmes.add(new Program("B", 35));
        channel2Programmes.add(new Program("CH", 130));
        channel2Programmes.add(new Program("DH", 80));
        channel2Programmes.add(new Program("EE", 100));
        channel2Programmes.add(new Program("G", 72));
        channel2Programmes.add(new Program("H", 76));
        channel2Programmes.add(new Program("J", 200));
        channel2Programmes.add(new Program("L", 90));

         channel1Handler = new Handler();
         channel2Handler = new Handler();
    }

    private void getViews(){
        channel1 = findViewById(R.id.channel_1_text);
        channel2 = findViewById(R.id.channel_2_text);
    }

    private void startTransmitting(){
        channel1.setText(channel1Programmes.get(channel1Iterator).getName());
        Log.d(TAG, "Channel 1 is playing program: " + channel1Programmes.get(channel1Iterator).getName());
        startTransmittingChannel1();


        channel2.setText(channel2Programmes.get(channel2Iterator).getName());
        Log.d(TAG, "Channel 2 is playing program: " + channel2Programmes.get(channel2Iterator).getName());
        startTransmittingChannel2();

    }

    private void startTransmittingChannel1(){
        channel1Handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                channel1.setText(publicitet.getName());
                Log.d(TAG, "Channel 1 is playing program: " + publicitet.getName());
                if(channel1Iterator == channel1Programmes.size() - 1){
                    channel1Iterator = 0;
                }else {
                    channel1Iterator++;
                }
                endProgramChannel1();
            }
        }, channel1Programmes.get(channel1Iterator).getTimeLengthMinutes()*COIF);

    }
    private void startTransmittingChannel2(){
        channel2Handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                channel2.setText(publicitet.getName());
                Log.d(TAG, "Channel 2 is playing program: " + publicitet.getName());
                if(channel2Iterator == channel2Programmes.size() - 1){
                    channel2Iterator = 0;
                }else {
                    channel2Iterator++;
                }
                endProgramChannel2();
            }
        }, channel2Programmes.get(channel2Iterator).getTimeLengthMinutes()*COIF);

    }

    private void endProgramChannel1(){
        channel1.postDelayed(new Runnable() {
            @Override
            public void run() {
                channel1.setText(channel1Programmes.get(channel1Iterator).getName());
                Log.d(TAG, "Channel 1 is playing program: " + channel1Programmes.get(channel1Iterator).getName());
                startTransmittingChannel1();
            }
        }, publicitet.getTimeLengthMinutes()*COIF);
    }

    private void endProgramChannel2(){
        channel2.postDelayed(new Runnable() {
            @Override
            public void run() {
                channel2.setText(channel2Programmes.get(channel2Iterator).getName());
                Log.d(TAG, "Channel 2 is playing program: " + channel2Programmes.get(channel2Iterator).getName());
                startTransmittingChannel2();
            }
        }, publicitet.getTimeLengthMinutes()*COIF);
    }
}
