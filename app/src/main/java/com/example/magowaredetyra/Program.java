package com.example.magowaredetyra;

public class Program {

    private String name;
    private int timeLengthMinutes;

    public Program(String name, int timeLengthMinutes){
        this.name = name;
        this.timeLengthMinutes = timeLengthMinutes;
    }

    public String getName() {
        return name;
    }

    public int getTimeLengthMinutes() {
        return timeLengthMinutes;
    }
}
